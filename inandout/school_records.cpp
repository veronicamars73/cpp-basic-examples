/*
Create a program that recieve 10 full names of students and print them in alphabetical order
*/

#include <iostream>
#include <algorithm>    // std::sort
#include <vector>       // std::vector

void swap(std::string *xp, std::string *yp)  
{  
    std::string temp = *xp;  
    *xp = *yp;  
    *yp = temp;  
}  
  
// A function to implement bubble sort  
void bubbleSort(std::string arr[]){  
    int i,j;
    for (i = 0; i < 9; i++) {      
	    // Last i elements are already in place  
	    for (j = 0; j < 9-i; j++)  {
	        if (arr[j] > arr[j+1]) {
	            swap(&arr[j], &arr[j+1]); 
	        } 
	    }
	}
}  

int main(int argc, char const *argv[]){
	
	std::string arr[10];
	std::string nome;

	for (int i = 0; i < 10; ++i){
		std::getline(std::cin, nome);
		arr[i] = nome;
	}

	bubbleSort(arr);
	for (int i = 0; i < 10; ++i){
		std::cout << arr[i] << std::endl;
	}

	return 0;
}
