/*

1.ª) Faça um programa que leia dois valores

e informe a média entre eles. (use float como tipo de dado).

*/

#include <iostream>

int main(int argc, char *argv[]){
	float n1,n2,m;

	std::cin >> n1;
	std::cin >> n2;

	std::cout << (n1+n2)/2 << std::endl;
}