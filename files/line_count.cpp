/*
Write a program in C++ to Find the Number of Lines in a Text File.
*/

#include <iostream>
#include <fstream>


int main(int argc, char *argv[]){
	std::fstream myfile;
	myfile.open("linhas.txt", std::ios::in);
	std::string temp;
	int contlines=0;
	if (myfile.is_open()){
		
		while(!myfile.eof()){
			
			std::getline(myfile, temp);
			contlines++;
		}
		myfile.close();
	}
	std::cout << "O arquivo continha " << contlines << " linhas" << std::endl;
}