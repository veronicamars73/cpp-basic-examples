#include "func.h"


#include <iostream>
#include <fstream>

int main(int argc, char *argv[]){
	std::ofstream record;
	record.open("employees.txt", std::ios::out);
	std::string temp;
	if (record.is_open()){
		runtheadding(record);
		record.close();
	}else{
		std::cerr << "Could not open the file." << std::endl;
	}
	return 0;
}