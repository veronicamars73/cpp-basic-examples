#include "func.h"


#include <iostream>
#include <fstream>

void printmenu(){
	std::cout << "1- Add Employee" << std::endl;
	std::cout << "2- Exit" << std::endl;
	std::cout << "Type the number of the option that you want:" << std::endl;
}

void getinfos(std::string* name, std::string* function){
	std::cout << "Type the name of the employee:" << std::endl;
	std::cin.ignore();
	std::getline(std::cin, *name);
	std::cout << "Type their function:" << std::endl;
	std::getline(std::cin, *function);
}

void writeinfile(std::string name, std::string function, std::ofstream &outfile){
	outfile << name << std::endl;
	outfile << function << std::endl;
	outfile  << std::endl;
}

void runtheadding(std::ofstream &outfile){
	int option;
	std::string name, function;
	do{
		printmenu();
		std::cin >> option;
		if (option==1){
			getinfos(&name, &function);
			writeinfile(name, function, outfile);
		}
	}while(option==1);
}