#ifndef FUNC_H
#define FUNC_H
#include <iostream>
#include <fstream>

void printmenu();
void getinfos(std::string* name, std::string* function);
void writeinfile(std::string name, std::string function, std::ofstream &outfile);
void runtheadding(std::ofstream &outfile);

#endif