/*
Write a program in C++ to write employees name and function in a file.
Follow this example:
Robert Elliot Nuñez
CEO

Eve Marie Spears
CFO

Bette Angella Fairbanks
Designer

*/


#include <iostream>
#include <fstream>

void printmenu(){
	std::cout << "1- Add Employee" << std::endl;
	std::cout << "2- Exit" << std::endl;
	std::cout << "Type the number of the option that you want:" << std::endl;
}

void getinfos(std::string* name, std::string* function){
	std::cout << "Type the name of the employee:" << std::endl;
	std::cin.ignore();
	std::getline(std::cin, *name);
	std::cout << "Type their function:" << std::endl;
	std::getline(std::cin, *function);
}

void writeinfile(std::string name, std::string function, std::ofstream &outfile){
	outfile << name << std::endl;
	outfile << function << std::endl;
	outfile  << std::endl;
}

void runtheadding(std::ofstream &outfile){
	int option;
	std::string name, function;
	do{
		printmenu();
		std::cin >> option;
		if (option==1){
			getinfos(&name, &function);
			writeinfile(name, function, outfile);
		}
	}while(option==1);
}


int main(int argc, char *argv[]){
	std::ofstream record;
	record.open("employees.txt", std::ios::out);
	std::string temp;
	if (record.is_open()){
		runtheadding(record);
		record.close();
	}else{
		std::cerr << "Could not open the file." << std::endl;
	}
	return 0;
}