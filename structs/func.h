#ifndef FUNC_H
#define FUNC_H
#include <iostream>
#include <fstream>

struct employee{
	std::string name;
	std::string function;
};

void printmenu();
void getinfos(employee* temp);
void writeinfile(employee temp, std::ofstream &outfile);
void runtheadding(std::ofstream &outfile);

#endif