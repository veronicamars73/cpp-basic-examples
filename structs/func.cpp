#include "func.h"


#include <iostream>
#include <fstream>

void printmenu(){
	std::cout << "1- Add Employee" << std::endl;
	std::cout << "2- Exit" << std::endl;
	std::cout << "Type the number of the option that you want:" << std::endl;
}

void getinfos(employee* temp){
	std::cout << "Type the name of the employee:" << std::endl;
	std::cin.ignore();
	std::getline(std::cin, (*temp).name);
	std::cout << "Type their function:" << std::endl;
	std::getline(std::cin, (*temp).function);
}

void writeinfile(employee temp, std::ofstream &outfile){
	outfile << temp.name << std::endl;
	outfile << temp.function << std::endl;
	outfile  << std::endl;
}

void runtheadding(std::ofstream &outfile){
	int option;
	employee temp;
	do{
		printmenu();
		std::cin >> option;
		if (option==1){
			getinfos(&temp);
			writeinfile(temp, outfile);
		}
	}while(option==1);
}